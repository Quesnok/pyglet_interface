import pyglet
from gamescreen import GameScreen
from player import Player
from gui import Containeritem
from map import Map






class Game(object):
    """docstring for Game."""
    def __init__(self):
        super(Game, self).__init__()
        print('starting game...')
        self.run = True
        print('starting player')
        self.player = Player(self,'TestUser')
        # self.player = Player(self,input('Enter your name...'))
        print('starting GameScreen')
        self.screen = GameScreen(self)
        print('starting Map')
        # self.engine = Engine()
        # self.map = self.engine.Map()
        self.map = Map()

        print('loading layout')
        self.screen.layout()
        print('loading player to game')
        self.player.startpos = self.player.load_pos()



current = Game()




def main():
    """docstring for mainloop"""
    while current.run:
        for window in pyglet.app.windows:
            window.switch_to()
            window.dispatch_events()
            window.dispatch_event('on_draw')
            window.flip()


if __name__ == "__main__":
    main()
