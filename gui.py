import pyglet
backgr = pyglet.image.load('res/background.png')

class Container(object):
    """docstring for Container."""
    def __init__(self, container = None, pos=False, mes=False):
        super(Container, self).__init__()
        self.container = container
        self.game = container.game
        self.x,self.y = pos if pos else (0,0)
        self.width,self.height = mes if mes else (25,25)
        self.sprites = list()

    def draw(self):
        """
        sets background if not already, draws the Container on the surface
        calls the draw() of its items
        """
        if not hasattr(self,'bg'): self.bg = backgr
        self.bg.width = self.width
        self.bg.height = self.height
        self.bg.blit(self.x,self.y)

        if hasattr(self,'items') and len(self.items) > 0:
            for item in self.items.values():
                item.draw()


    def on_mouse_left(self,x,y):
        for obj in self.sprites:
            if obj.x <= x <= obj.x + obj.width \
            and obj.y <= y <= obj.y+obj.height:
                obj.parent.action()




    def add(self,key,item):
        if not hasattr(self,'items'): self.items = dict()
        setattr(item,'id',key)
        if not hasattr(item,'container'): item.container = self
        self.items[key] = item


class Containeritem():
    """docstring for Containeritem."""
    def __init__(self,container = None,img=False,text=False,pos = False):
        ## TODO: **qwargs
        self.img = img
        self.text = text
        if pos: self.px,self.py = pos #relative position
        if container is not None:
            self.container = container
            self.game = container.game
        self.sprite = None
        self.set()
        # print(self.__dict__)

    def draw(self):
        """
        draws img and if given, label on surface
        """
        if self.img: self.img.blit(self.x,self.y)
        if hasattr(self,'label'): self.label.draw()

    def set(self):
        """
        sets position,
        sets img position and Label, if given
        """
        if not hasattr(self,'x'):
            if hasattr(self,'px'):
                self.x = self.px + self.container.x
                self.y = self.py + self.container.y
            if hasattr(self,'coord'):
                print('has coord')
                self.x,self.y = self.coord

        if self.img:
            self.img.x = self.x
            self.img.y = self.y
            sprite = pyglet.sprite.Sprite(self.img,self.x,self.y)
            sprite.parent = self
            if sprite not in self.container.sprites:
                self.container.sprites.append(sprite)
                self.sprite = sprite

        if hasattr(self,'text') and self.text:
            self.label = pyglet.text.Label(str(self.text),
                  font_name='Times New Roman',
                  font_size=14,
                  x=self.x+5, y=self.y+15)


    def action(self):
        pass



class Info(Containeritem):
    """docstring for Info."""
    def __init__(self, arg):
        super(Info, self).__init__()
        self.infos = arg
        #dirty
        self.x = 625
        self.y = 450
        self.width = 150
        self.text,self.img = self.show_info()
        if self.img: self.img.y = self.y - 500

        self.label = pyglet.text.Label(self.text,
              font_name='Times New Roman',
              font_size=14,
              x=self.x, y=self.y,width = self.width,multiline=True)





    def show_info(self):
        # TODO: delete last info from info.items
        text = str()
        if 'tile' in self.infos['type']:
            img = self.infos['img']
            text+='Tile'+str(self.infos['coord'])
            text+= '\n'

            if self.infos['building']:
                text+='has Building '+str(self.infos['building'])
                text+= '\n'
            else:
                text+='has no building'
                text+= '\n'

            if self.infos['owner']:
                text+='owned by '+str(self.infos['owner'].name)
                text+= '\n'
            else:
                text+='owned by City'
                text+= '\n'

            text+= '\n'
        return text,img
