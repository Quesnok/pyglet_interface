import pyglet
from pyglet.window import mouse
from gui import Container, Containeritem
from structure import Tile


mainbackgr = pyglet.image.load('res/mainbg.png')

class GameScreen(pyglet.window.Window,Container):
    """docstring for GameScreen."""
    def __init__(self,game):
        super(GameScreen, self).__init__(width=800,height=600,caption='SimQue.py')
        print('loading gamescreen...')
        self.id = 'gamescreen'

        self.game = game

        self.bg = pyglet.image.load('res/mainbg.png')
        self.x = 0
        self.y = 0


    def on_draw(self):
        self.clear()
        self.draw()

    def on_mouse_press(self,x,y,button,modifiers):
       if button == mouse.LEFT:
            for obj in self.items.values():
                if obj.x <= x <= obj.x + obj.width \
                and obj.y <= y <= obj.y+obj.height: obj.on_mouse_left(x,y)

    def layout(self):
        # TODO: load this from .json
        print('layouting screen...')
        self.add('mapbox',Container(self,mes=(500,500)))

        box = self.items['mapbox']
        map = self.game.map.map
        for item in map:
            coord = map[item]['coord']
            tile = Tile(container=box,coord = coord)
            map[item]['tile'] = tile
            box.add(item,tile)


            # print(map[item])


        self.add('infobar',Container(self,pos=(0,525),mes=(800,65)))
        box = self.items['infobar']
        box.bg = pyglet.image.load('res/infobar/bg_infobar.png') #custom background
        for x, key in enumerate(self.game.player.stats):
            entry = self.game.player.stats[key]
            img = pyglet.image.load('res/infobar/'+key+'.png')
            box.add(str(key),Containeritem(box,img,text=str(entry),pos=(x*75,0)))
            setattr(box.items[key],'text',entry)
            box.items[key].set()

        self.add('info',Container(self,pos =(625,0),mes=(600,75)))
