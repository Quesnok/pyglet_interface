import random
from structure import Living, Nucleus


class Player(object):
    """docstring for Player."""
    def __init__(self, game, arg):
        super(Player, self).__init__()
        self.game = game
        self.name = arg
        self.startpos = None
        self.stats = {
                        'player': self.name,
                        'gold' : 5,
                        'pop' : 5,
                        'happy': 0,
                        'next' : self.startpos
                        }



    def load_pos(self):
        pos = random.randint(0,99)
        own = self.game.map.map[pos]

        own['building'] = Living(own['tile'])

        # own['building'] = Nucleus(own['tile'])
        own['owner'] = self
        own['building'].type = 'Start'
        self.startpos = own
        print(own)
        print(own['building'].__dict__)
