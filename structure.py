
import pyglet
from gui import Containeritem, Info

empty = pyglet.image.load('res/tile-empty.png')
IMG_BUILDINGS = {'nuc' : pyglet.image.load('res/nuc.png')}

class Tile(Containeritem):
    """docstring for Tile."""
    def __init__(self,container,coord):
        super(Tile, self).__init__(container,pos=(coord[0]*51,coord[1]*51))

        self.constructed = False
        self.building = False
        self.coord = coord
        self.img = pyglet.image.load('res/tile-empty.png')
        self.text = str(coord)
        self.game = container.game
        if coord == (0,0): print(self.container.__dict__)





    def r_values(self):
        # TODO: return config dict?
        r_dict = {'type' : 'tile',
                'coord' : self.coord,
                'building' : self.building.type if self.building else self.building, #not clean
                'owner' : self.game.map.map[int(''.join(map(str,self.coord)))]['owner'],
                'img' : self.building.img if self.building else self.img}
        return r_dict



    def action(self):
        print('click\n',self.__dict__)
        info = Info(self.r_values())
        self.container.game.screen.items['info'].items = dict()
        self.container.game.screen.items['info'].add('info',info)

    def draw(self):
        super().draw()
        if self.building:
            self.building.draw()
        if not self.sprite: self.set()


class Building():
    """docstring for Building."""
    def __init__(self,tile):
        print(tile)
        self.tile = tile
        self.tile.building = self
        self.img = False
        self.type = None


    def set_image(self,img):
        """ looks up the IMG_BUILDINGS dict for image,
        currently only used for players startpos """
        image = IMG_BUILDINGS[img]
        image.width = 40
        image.height = 40


        return image



    def draw(self):
        if self.img: self.img.blit(self.tile.x+5,self.tile.y+5)



class Living(Building):
    """docstring for Living."""

    def __init__(self, tile):
        super(Living, self).__init__(tile=tile)
        self.tile = tile
        self.household = False
        self.img = self.set_image('nuc')
        self.draw()


class Nucleus(Building):
    """docstring for Nucleus."""

    def __init__(self, tile):
        super(Nucleus, self).__init__(tile=tile)
        self.tile = tile
        self.img = self.set_image('nuc')
        self.draw()
