"""file in-/output utils"""

import os
import sys
import json
import collections

import utils


def load_json(path):
    """Tries to load every json file in a directory

        :param path: load jsons from this path
        :type path: str
        :return: loaded jsons
        :rtype: dict
    """
    path = os.path.join(utils.base_dir + path)
    dirlist = os.listdir(path)
    json_cache = dict()

    for file in dirlist:
        key = os.path.splitext(file)[0]
        typ = os.path.splitext(file)[1]
        if typ == '.json':
            with open(path + '/' + file) as file:
                json_cache[key] = json.load(file)
    return json_cache

def create_path(path):
    """Create a path if it doesn't already exist"""
    path = os.path.join(utils.base_dir + path)

    if not os.path.exists(path):
        os.makedirs(path)

def load_file(fname):
    """open a file to read"""
    return open(utils.base_dir + fname, 'r')

def dump_file(fname):
    """open a file to write"""
    return open(utils.base_dir + fname, 'w')

def load_probs(fname, sort=False):
    """Return an arbitrary probability table loaded from file fname

        The table has the form:
        prob_0, [list of data_values_0]
        prob_1, [list of data_values_1]
        ...

        A check is made to ensure that probabilities sum to 1.0

        probabilities are sorted from most to least frequent for future efficiency
    """
    file = load_file(fname)
    tmp = []
    for line in file:
        if line[0] is '#':
            continue
        line = line.strip().split(' ')
        tmp.append([float(line[0]), line[1:]])

    if abs(sum(list(zip(*tmp))[0])-1.0) > 0.00001:
        sys.stderr.write("Probs in file %s don't sum to 1.0") % fname
        exit(1)

    if sort:
        tmp.sort(reverse=True)

    file.close()
    return tmp

def load_probs_new(fname, sort=False):
    """Return an arbitrary probability table loaded from file fname

        The table has the form:
            prob_0, [list of data_values_0]
            prob_1, [list of data_values_1]
            ...

        A check is made to ensure that probabilities sum to 1.0
        probabilities are sorted from most to least frequent for future efficiency
        lines beginning with '#' are skipped (comments)
        lines beginning with '@' force the beginning of a new subtable
    """
    file = load_file(fname)
    tmp = []
    current_t = []

    for line in file:
        if line[0] is '#':
            continue
        if line[0] is '@':
            tmp.append(current_t[:])
            current_t = []
        else:
            line = line.strip().split(' ')
            current_t.append([float(line[0]), line[1:]])
    tmp.append(current_t[:])
    file.close()

    for current_t in tmp:
        if abs(sum(list(zip(*current_t))[0])-1.0) > 0.00001:
            sys.stderr.write("Probs in file %s don't sum to 1.0" % fname)
            exit()

        if sort:
            current_t.sort(reverse=True)

    if len(tmp) == 1:
        return tmp[0]

    return tmp

def load_age_rates(fname):
    """Load age-dependent rates from file fname.

        File has the format:
            age rate_1 rate_2 rate_3 ... etc
    """
    file = load_file(fname)
    tmp = []

    for line in file:
        if line[0] is '#':
            continue
        tmp.append([eval(x) for x in line.strip().split(' ')])

    file.close()
    return tmp

def load_prob_list(fname):
    """Loads a sequence of probabilities/rates and returns them as a list.

        For use in specifying, e.g., time-varying marriage rates.

        File has the format:
            value_1
            value_2
            value_3
            ...
    """
    file = load_file(fname)
    tmp = []

    for line in file:
        if line[0] is '#':
            continue
        line = line.strip().split(' ')
        r = eval(line[0])
        tmp.append(float(line[0]))
    file.close()
    return tmp

def load_prob_tables(fname):
    """Load a set of probability tables

        form:
            data_value, prob_0, prob_1, prob_2, ...
            ...

        Used for time-varying probabilities (i.e., fertility by age).

        Returns a dictionary keyed by data_value, with a normal
        prob_0, [data_value] table (as below).

        This isn't particularly optimal, but allows current sampling functions
        to be used unchanged.
    """
    file = load_file(fname)
    tmp = collections.defaultdict(list)

    for line in file:
        if line[0] is '#':
            continue
        line = line.strip().split(' ')
        for i, val in enumerate(line[1:]):
            tmp[int(i)].append([float(val), [int(line[0])]])

    file.close()
    return tmp

def load_prob_hh(fname):
    """load probs household"""
    file = load_file(fname)
    rawdata = []

    for line in file.readlines()[2:]:
        # all instances of '10+' become '10'
        line2 = [int(x.strip('+')) for x in line.split()]
        # remove syd and melb columns, collapse working and non-working
        line3 = line2[:2]+[line2[2]+line2[3]]+[line2[6]]
        # collapse all occurrences of >10 to 10
        if line3[0] >= 10:
            line3[0] = 10
        if line3[1] >= 10:
            line3[1] = 10
        if line3[2] >= 10:
            line3[2] = 10
        rawdata.append(line3)

    file.close()
    return rawdata

def write_prob_hh(fname, data):
    """write household probs"""
    file = dump_file(fname)
    for i in range(11):
        for j in range(11):
            for k in range(11):
                if data[i][j][k] > 0:
                    file.write("%g %d %d %d\n" % (data[i][j][k], i, j, k))
    file.close()
