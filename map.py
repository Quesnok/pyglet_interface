

class Map(object):
    """docstring for Map."""
    def __init__(self):
        super(Map, self).__init__()
        self.map = self.load_map()
        # print(self.map)


    def load_map(self):
        print('loading map...')
        map_size = 10
        map = dict()
        id = 0
        for x in range(map_size):
            for y in range(map_size):
                coord = x,y
                map[id] = {'coord' : coord, 'tile' : None, 'owner' : False, 'building' : False}
                id+=1
        return map
